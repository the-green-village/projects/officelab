# OfficeLab

The OfficeLab projects reads data from Climatics through **WebSocket**.

https://climatics.nl/

## REST API

See the documentation in [docs.pdf](docs.pdf) and [docs-auth.pdf](docs-auth.pdf).

```
curl -XGET https://www.climatics.nl/api/location/48/sensor -H "Authorization: Basic $CLIMATICS_APIKEY" | jq . > sensors.json
```

## WebSocket client

```
pip install websocket-client
python3 subscribe_test.py --api-key $CLIMATICS_APIKEY --location-id $CLIMATICS_LOCATIONID
```

## Kafka producer

### Run with Docker

```
export DOCKER_ID_USER="salekd"
docker login https://index.docker.io/v1/

docker build . -t officelab-climatics
docker tag officelab-climatics $DOCKER_ID_USER/officelab-climatics:1.1.0
docker push $DOCKER_ID_USER/officelab-climatics:1.1.0

docker run -v $(pwd)/officelab.cfg:/officelab.cfg officelab-climatics
```

### Deploy on Kubernetes

```
# Create ConfigMap with the configuration file.
kubectl delete configmap officelab-climatics -n ccloud
kubectl create configmap officelab-climatics -n ccloud \
  --from-file=officelab.cfg=officelab.cfg

# Deploy a pod.
kubectl delete deploy officelab-climatics -n ccloud
kubectl apply -f officelab-climatics-dep.yaml

kubectl logs deploy/officelab-climatics -n ccloud
```
