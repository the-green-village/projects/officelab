# -*- coding: utf-8 -*-
import base64
import json
import logging
import pathlib
from configparser import ConfigParser

import requests
import websocket
from confluent_kafka import SerializingProducer
from confluent_kafka.schema_registry import SchemaRegistryClient
from confluent_kafka.schema_registry.avro import AvroSerializer

logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.WARNING
)
logger = logging.getLogger(__file__)

# Read config file
config = ConfigParser()
config.read(pathlib.Path(__file__).parent.resolve() / "officelab.cfg")
climatics_location = config.getint("Climatics", "location")
climatics_key = config.get("Climatics", "key")
bootstrap_servers = config.get("Kafka", "bootstrap_servers")
api_key = config.get("Kafka", "api_key")
api_secret = config.get("Kafka", "api_secret")
topic = config.get("Kafka", "topic")
schema_registry_url = config.get("Kafka", "schema_registry_url")
schema_registry_auth = config.get("Kafka", "schema_registry_auth")


conf = {"url": schema_registry_url, "basic.auth.user.info": schema_registry_auth}
schema_registry_client = SchemaRegistryClient(conf)

schema_str = '{"type":"record","name":"GreenVillageRecord","namespace":"thegreenvillage","fields":[{"name":"project_id","type":"string","doc":"Globally unique id for the project."},{"name":"application_id","type":"string","doc":"Unique id for the application/use case."},{"name":"device_id","type":"string","doc":"Unique id for the device."},{"name":"timestamp","type":"long","doc":"Timestamp (milliseconds since unix epoch) of the measurement."},{"name":"measurements","type":{"type":"array","items":{"type":"record","name":"measurement","fields":[{"name":"measurement_id","type":"string","doc":"Unique id for the measurement."},{"name":"value","type":["null","boolean","int","long","float","double","string"],"doc":"Measured value."},{"name":"unit","type":["null","string"],"doc":"Unit of the measurement.","default":null},{"name":"measurement_description","type":["null","string"],"doc":"Measurement description.","default":null}]}},"doc":"Array of measurements."},{"name":"project_description","type":["null","string"],"doc":"Project description.","default":null},{"name":"application_description","type":["null","string"],"doc":"Application/use case description.","default":null},{"name":"device_description","type":["null","string"],"doc":"Device description.","default":null},{"name":"device_manufacturer","type":["null","string"],"doc":"Device manufacturer.","default":null},{"name":"device_type","type":["null","string"],"doc":"Device type.","default":null},{"name":"device_serial","type":["null","string"],"doc":"Device serial number.","default":null},{"name":"location_id","type":["null","string"],"doc":"Unique id for the location.","default":null},{"name":"location_description","type":["null","string"],"doc":"Location description.","default":null},{"name":"latitude","type":["null","float"],"doc":"Latitude in decimal degrees of the device location (GPS coordinates).","default":null},{"name":"longitude","type":["null","float"],"doc":"Longitude in decimal degrees of the device location (GPS coordinates).","default":null},{"name":"altitude","type":["null","float"],"doc":"Altitude in meters above the mean sea level.","default":null}]}'
conf = {"auto.register.schemas": False}
avro_serializer = AvroSerializer(schema_registry_client, schema_str, conf=conf)

# Create Producer instance
producer = SerializingProducer(
    {
        "client.id": "officelab-climatics-producer",
        "bootstrap.servers": bootstrap_servers,
        "sasl.mechanisms": "PLAIN",
        "security.protocol": "SASL_SSL",
        "sasl.username": api_key,
        "sasl.password": api_secret,
        "ssl.ca.location": "/etc/ssl/certs/ca-certificates.crt",
        "value.serializer": avro_serializer,
        "acks": "1",
        # If you don’t care about duplicates and ordering:
        "retries": 10000000,
        "delivery.timeout.ms": 2147483647,
        "max.in.flight.requests.per.connection": 5
        #        # If you don’t care about duplicates but care about ordering:
        #        'retries': 10000000,
        #        'delivery.timeout.ms': 2147483647,
        #        'max.in.flight.requests.per.connection': 1
        #        # If you care about duplicates and ordering:
        #        'retries': 10000000,
        #        'delivery.timeout.ms': 2147483647,
        #        'enable.idempotence': True
    }
)


# Get the sensor overview from Climatics and
# create a lookup dictionary that will be used
# to format the messages received through WebSocket
# into the generic format expected by Kafka.
r = requests.get(
    f"https://www.climatics.nl/api/location/{climatics_location}/sensor",
    headers={"Authorization": f"Basic {climatics_key}"},
)
sensors = {}
for item in r.json()["items"]:
    # device_id = str(item["port"]["device"]["id"]) if item["port"] else str(item["id"])
    # device_description = item["port"]["device"]["name"] if item["port"] else item["name"]
    device_id = (
        item["equips"][0]["equip"]["name"]
        if item["equips"]
        else item["name"]
    )
    device_description = (
        str(item["equips"][0]["equip"]["id"])
        if item["equips"]
        else str(item["id"])
    )
    location_id = str(item["rooms"][0]["room"]["id"]) if item["rooms"] else None
    location_description = item["rooms"][0]["room"]["name"] if item["rooms"] else None

    # Measurements from the weather station
    if item["id"] in [13029, 29942, 33885, 33886, 33887, 33888, 33901]:
        application_id = "weather"
    else:
        application_id = "climatics"

    sensors[item["id"]] = {
        "measurement_id": item["name"],
        "measurement_description": str(item["id"]),
        "unit": item["unit"],
        "application_id": application_id,
        # "device_id": str(item["id"]),
        # "device_description": item["name"],
        "device_id": device_id,
        "device_description": device_description,
        "location_id": location_id,
        "location_description": location_description,
    }


# Optional per-message on_delivery handler (triggered by poll() or flush())
# when a message has been successfully delivered or
# permanently failed delivery (after retries).
def acked(err, msg):
    """Delivery report handler called on
    successful or failed delivery of message
    """
    if err is not None:
        logger.error("Failed to deliver message: {}".format(err))
    else:
        logger.info(
            "Produced record to topic {} partition [{}] @ offset {}".format(
                msg.topic(), msg.partition(), msg.offset()
            )
        )

def on_message(ws, message):
    """Produce the message received through WebSocket to Kafka.

    Example message:
        {"type":"publish","name":"mbo.sensor.update","data":{"id":14232,"location":{"id":48},"current_value":{"value":0.1,"timestamp":1588101739}}}
    """

    msg = json.loads(message)
    idx = msg["data"]["id"]

    value = {
        "timestamp": msg["data"]["current_value"]["timestamp"] * 1000,
        "project_id": "officelab-climate",
        "application_id": sensors[idx]["application_id"],
        "device_id": sensors[idx]["device_id"],
        "measurements": [
            {
                "measurement_id": sensors[idx]["measurement_id"],
                "measurement_description": sensors[idx]["measurement_description"],
                "unit": sensors[idx]["unit"],
                "value": float(msg["data"]["current_value"]["value"]),
                "type": "FLOAT",
            }
        ],
        "location_id": sensors[idx]["location_id"],
        "location_description": sensors[idx]["location_description"],
    }
    logger.debug(json.dumps(value, sort_keys=True, indent=4))

    # Produce asynchronously.
    producer.produce(topic=topic, value=value, on_delivery=acked)

    # Trigger any available delivery report callbacks from previous produce() call.
    producer.poll(0)


def make_on_open(location_id):
    def on_open(ws):
        ws.send(
            json.dumps(
                {
                    "type": "subscribe",
                    "name": "mbo.sensor.update",
                    "data": {"location": {"id": location_id}},
                }
            )
        )

    return on_open


keyenc = base64.b64encode(climatics_key.encode("utf-8")).decode("utf-8")
ws = websocket.WebSocketApp(
    f"wss://climatics.nl/ws?api_key={keyenc}",
    on_message=on_message,
    on_open=make_on_open(climatics_location)
    #    on_error=on_error,
    #    on_close=on_close
)

try:
    ws.run_forever()
except KeyboardInterrupt:
    logger.error("Aborted by user")
finally:
    producer.flush()
