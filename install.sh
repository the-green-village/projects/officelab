#!/bin/bash
# mo officelab.cfg_template > officelab.cfg

kubectl delete configmap officelab-climatics -n tud-gv
kubectl create configmap officelab-climatics -n tud-gv \
  --from-file=officelab.cfg=officelab.cfg

kubectl delete deploy officelab-climatics -n tud-gv
kubectl apply -f officelab-climatics-dep.yaml
