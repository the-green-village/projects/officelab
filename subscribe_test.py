#!/usr/bin/env python3
# -*- coding: utf-8 -*-
""" A websocket subscribe test script.

This script subscribes to all sensor updates in a given location.
You'll need to provide a api key when running this script, and a location id.

To run this script, set up a virtual environment with python 3 like this:

    $ python3 -m venv .env
    $ source .env/bin/activate
    $ pip install websocket-client
    $ python3 subscribe_test.py --api-key YOURAPIKEY --location-id YOULOCATIONID

"""
import argparse
import base64
import json
import sys

import websocket


def parse_api_key(key):
    print(base64.b64encode(key.encode("utf-8")).decode("utf-8"))
    return base64.b64encode(key.encode("utf-8")).decode("utf-8")


def on_message(ws, message):
    print("--- RECEIVED MESSAGE ---")

    msg = json.loads(message)

    print("Parsed Message:")
    print("  Sensor Value: {}".format(msg["data"]["current_value"]["value"]))
    print("  Sensor ID: {}".format(msg["data"]["id"]))

    print("\nRaw JSON Message:")
    print(message)

    print("--- END MESSAGE ---")


def on_error(ws, error):
    print(error)


def on_close(ws):
    print("CLOSED")


def make_on_open(location_id):
    def on_open(ws):
        print(
            "--- SUBSCRIBING TO ALL SENSOR UPDATES IN LOCATION {} ---".format(
                location_id
            )
        )

        ws.send(
            json.dumps(
                {
                    "type": "subscribe",
                    "name": "mbo.sensor.update",
                    "data": {"location": {"id": location_id}},
                }
            )
        )

    return on_open


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Subscribe to all sensor updates for a location."
    )
    parser.add_argument(
        "--api-key", type=str, default=None, help="which api key to use"
    )
    parser.add_argument(
        "--location-id",
        type=int,
        default=None,
        help="id of location that the sensor is in",
    )
    args = parser.parse_args()

    if args.api_key is None or args.location_id is None:
        print("Please provide an api key and a location id (see --help).")
        sys.exit(1)

    print(parse_api_key(args.api_key))
    ws = websocket.WebSocketApp(
        # 'wss://staging.climatics.nl/ws?api_key={}'.format(parse_api_key(args.api_key)),
        "wss://climatics.nl/ws?api_key={}".format(parse_api_key(args.api_key)),
        on_error=on_error,
        on_message=on_message,
        on_close=on_close,
        on_open=make_on_open(args.location_id),
    )

    ws.run_forever()
