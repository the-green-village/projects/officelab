FROM python:3.7-slim

ADD requirements.txt /

RUN pip install --upgrade pip && \
    pip install -r requirements.txt

ADD serializing-producer.py /

CMD [ "python", "serializing-producer.py" ]
